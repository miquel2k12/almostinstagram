﻿using AlmostInstagram.Entities;
using AlmostInstagram.Models;
using AlmostInstagram.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlmostInstagram.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        private IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet("{id}")]
        public UserDataModel Get(long id)
        {
            return _userService.GetUser(id);
        }

        [HttpGet]
        public List<UserDataModel> Find([FromQuery]string search)
        {
            return _userService.GetUsers(search);
        }

        [HttpGet("{id}/Follow/{userId}")]
        public User FollowUser(long id, long userId)
        {
            return _userService.FollowUser(id, userId);
        }

        [HttpGet("{id}/Unfollow/{userId}")]
        public User UnfollowUser(long id, long userId)
        {
            return _userService.UnfollowUser(id, userId);
        }
    }
}
