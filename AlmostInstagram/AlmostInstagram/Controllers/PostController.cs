﻿using AlmostInstagram.Entities;
using AlmostInstagram.Models;
using AlmostInstagram.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlmostInstagram.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class PostController : Controller
    {
        private IPostService _postService;

        public PostController(IPostService postService)
        {
            _postService = postService;
        }

        [HttpGet]
        public List<PostModel> GetPosts([FromQuery]long userId, [FromQuery]int pageSize, [FromQuery]int pageNbr)
        {
            return _postService.GetPosts(userId, pageSize, pageNbr);
        }

        [HttpGet("User")]
        public List<PostModel> GetUserPosts([FromQuery]long userId)
        {
            return _postService.GetUserPosts(userId);
        }

        [HttpPost]
        public Post CreatePost([FromBody]PostFormModel model)
        {
            return _postService.CreatePost(model);
        }

        [HttpPut("Comment")]
        public Comment AddComment([FromBody]CommentAddModel model)
        {
            return _postService.AddComment(model);
        }

        [HttpPut("Like")]
        public bool AddOrRemoveLike([FromBody]PostLikeModel model)
        {
            return _postService.AddOrRemoveLike(model);
        }
    }
}
