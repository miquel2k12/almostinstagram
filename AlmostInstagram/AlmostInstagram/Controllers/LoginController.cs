﻿using AlmostInstagram.Entities;
using AlmostInstagram.Models;
using AlmostInstagram.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlmostInstagram.Controllers
{
    [Route("api/[controller]")]
    public class LoginController : Controller
    {
        private ILoginService _loginService;

        public LoginController(ILoginService loginService)
        {
            _loginService = loginService;
        }

        [HttpPost]
        public UserModel Post([FromBody]LoginModel model)
        {
            return _loginService.Login(model);
        }

        [HttpPut]
        public User Put([FromBody]RegistrationModel model)
        {
            return _loginService.Register(model);
        }

        [HttpGet("{userName}")]
        public bool CheckUserNameAvailable(string userName)
        {
            return _loginService.CheckIsUserNameAvailable(userName);
        }
    }
}
