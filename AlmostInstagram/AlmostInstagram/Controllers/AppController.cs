﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using AlmostInstagram.Services;
using AlmostInstagram.Utils;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace AlmostInstagram.Controllers
{
    [Route("api/[controller]")]
    public class AppController : Controller
    {
        private IAppService _appService;

        public AppController(IAppService appService)
        {
            _appService = appService;
        }

        [HttpGet("{culture}/Resources")]
        public ContentResult JsonResources(string culture)
        {
            var resources = new JavascriptResourceHelper.JavaScriptResources(new System.Globalization.CultureInfo(culture));
            
            var result = JsonConvert.SerializeObject(resources);

            return Content(result);
        }
    }
}
