﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TypeLite;

namespace AlmostInstagram.Models
{
    [TsClass]
    public class CommentModel
    {
        public string Text { get; set; }
        public UserDataModel User { get; set; }
    }
}
