﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TypeLite;

namespace AlmostInstagram.Models
{
    [TsClass]
    public class PostFormModel
    {
        public int UserId { get; set; }
        public string FileBytes { get; set; }
    }
}
