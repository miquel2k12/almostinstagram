﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TypeLite;

namespace AlmostInstagram.Models
{
    [TsClass]
    public class UserDataModel
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public List<PostModel> Posts { get; set; } = new List<PostModel>();
        public List<UserDataModel> ObservedUsers { get; set; } = new List<UserDataModel>();
    }
}
