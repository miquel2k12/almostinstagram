﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TypeLite;

namespace AlmostInstagram.Models
{
    [TsClass]
    public class UserModel
    {
        public string UserName { get; set; }
        public string Token { get; set; }
        public long Id { get; set; }
    }
}
