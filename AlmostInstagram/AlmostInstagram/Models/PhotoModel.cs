﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TypeLite;

namespace AlmostInstagram.Models
{
    [TsClass]
    public class PhotoModel
    {
        public long Id { get; set; }
        public byte[] FileBytes { get; set; }
    }
}
