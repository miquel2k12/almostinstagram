﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TypeLite;

namespace AlmostInstagram.Models
{
    [TsClass]
    public class PostModel
    {
        public long Id { get; set; }
        public PhotoModel Photo { get; set; }
        public UserDataModel User { get; set; }
        public DateTime Date { get; set; }
        public List<CommentModel> Comments { get; set; }
        public List<LikeModel> Likers { get; set; }
    }
}
