﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TypeLite;

namespace AlmostInstagram.Models
{
    [TsClass]
    public class CommentAddModel
    {
        public long UserId { get; set; }
        public long PostId { get; set; }
        public string Text { get; set; }
    }
}
