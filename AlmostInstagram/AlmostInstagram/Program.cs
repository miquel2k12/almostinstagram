﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Builder;
using System.Threading;
using System.Globalization;
using TypeLite;
using TypeLite.Net4;
using System.Text.RegularExpressions;

namespace SIMAX
{
    public class Program
    {
        public static void Main(string[] args)
        {

#if DEBUG
            GenerateModelsForTypeScript();
#endif

            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-us");
            var host = new WebHostBuilder()
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseIISIntegration()
                .UseStartup<Startup>()
                .Build();

            host.Run();
        }

        // Warn: for developers only!
        public static void GenerateModelsForTypeScript()
        {
            var ts = TypeScript.Definitions()
                .ForLoadedAssemblies()
                .WithMemberFormatter((identifier) =>
                   Char.ToLower(identifier.Name[0]) + identifier.Name.Substring(1)
                )
                .WithModuleNameFormatter((identifier) => identifier.Name = "AlmostInstagram");
            
            var tsOutput = ts.Generate(TsGeneratorOutput.Properties);
            var tsOutputLines = tsOutput.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries).ToList();
            tsOutputLines[0] = "// File is generated :)";
            tsOutputLines.RemoveAt(tsOutputLines.Count - 1);
            var clearOutput = String.Join(Environment.NewLine, tsOutputLines.ToArray()).Replace("\t\t", "_").Replace("\t", "").Replace("_", "\t").Replace("interface", "export class").Replace("AlmostInstagram.", "").Replace("}\r\ndeclare module AlmostInstagram {\r\n", "");
            clearOutput = Regex.Replace(clearOutput, 
                @"KeyValuePair\<(?<k>[^\,]+),(?<v>[^\,]+)\>\[\];", 
                m => "{[key: " + m.Groups["k"].Value + "]: " + m.Groups["v"].Value + "};", 
                RegexOptions.Multiline);
            File.WriteAllText(Directory.GetCurrentDirectory() + @"..\..\AlmostInstagramClient\app\modules\api\api.models.ts", clearOutput);
        }
    }
}
