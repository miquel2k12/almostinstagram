﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AlmostInstagram.Entities
{
    public class Comment
    {
        public long Id { get; set; }
        [Required]
        public string Text { get; set; }

        [Required]
        public virtual User User { get; set; }
        public long? UserId { get; set; }

        [Required]
        public virtual Post Post { get; set; }
    }
}
