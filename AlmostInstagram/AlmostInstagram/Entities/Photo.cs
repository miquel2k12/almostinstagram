﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AlmostInstagram.Entities
{
    public class Photo
    {
        public long Id { get; set; }
        [Required]
        public byte[] FileBytes { get; set; }

        [Required]
        public virtual Post Post { get; set; }
        public long? PostId { get; set; }
    }
}
