﻿using AlmostInstagram.Utils;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AlmostInstagram.Entities.DataAccess
{
    public static class MigrationConfiguration
    {
        private static User User3 = new User()
        {
            FirstName = "Super",
            LastName = "User",
            UserName = "super.user",
            Password = Common.Sha256("1234")
        };

        private static User User2 = new User()
        {
            FirstName = "Katarzyna",
            LastName = "Toporek",
            UserName = "katarzyna.toporek",
            Password = Common.Sha256("1234")
        };

        private static User User1 = new User()
        {
            FirstName = "Michał",
            LastName = "Dziwoki",
            UserName = "michal.dziwoki",
            Password = Common.Sha256("1234"),
            ObservedUsers = new List<User>(new User[] { User2, User3 }),
            Posts = new List<Post>()
            {
                new Post()
                {
                    User = User1,
                    UploadDate = DateTime.Now,
                    Comments = new List<Comment>()
                    {
                        new Comment()
                        {
                            Text = "Komentarz XxxxxX",
                            User = User2
                        },
                        new Comment()
                        {
                            Text = "Komentarz xDDDD",
                            User = User3
                        }
                    },
                    Photo = new Photo()
                    {
                        FileBytes = File.ReadAllBytes("Data/photo1.jpg")
                    }
                }
            }
        };
        
        private static Comment Comment1 = new Comment()
        {
            Post = Post1,
            Text = "Komentarz 1",
            User = User1
        };

        private static Post Post1 = new Post()
        {
            User = User2,
            Comments = new List<Comment>(new Comment[] { Comment1 }),
            Photo = new Photo()
            {
                FileBytes = File.ReadAllBytes("Data/photo1.jpg")
            },
            UploadDate = DateTime.Now
    };

        public static void Seed(this DataContext context)
        {
            // Perform database delete and create
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();

            AddUsers(context);
            AddPosts(context);
            AddComments(context);
            AddPhotos(context);

            // Save changes and release resources
            context.SaveChanges();
            context.Dispose();
        }

        private static void AddUsers(DataContext context)
        {
            context.AddRange(User1, User2, User3);
        }

        private static void AddComments(DataContext context)
        {
            context.AddRange(Comment1);
        }

        private static void AddPosts(DataContext context)
        {
            context.AddRange(Post1);
        }

        private static void AddPhotos(DataContext context)
        {
            
        }
    }
}
