﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AlmostInstagram.Entities
{
    public class Post
    {
        public long Id { get; set; }

        [Required]
        public virtual User User { get; set; }
        public long? UserId { get; set; }
        
        public virtual Photo Photo { get; set; }
        public long? PhotoId { get; set; }

        public virtual IEnumerable<Comment> Comments { get; set; }
        public long? CommentId { get; set; }

        public virtual IEnumerable<Like> Likers { get; set; }
        public long? LikerId { get; set; }

        public DateTime UploadDate { get; set; }
    }
}
