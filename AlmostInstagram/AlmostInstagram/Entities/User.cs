﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AlmostInstagram.Entities
{
    public class User
    {
        public long Id { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Password { get; set; }

        public virtual IList<User> ObservedUsers { get; set; }
        public virtual User ThisUser { get; set; }
        public long? ObservedUserId { get; set; }

        public virtual IEnumerable<Post> Posts { get; set; }
    }
}
