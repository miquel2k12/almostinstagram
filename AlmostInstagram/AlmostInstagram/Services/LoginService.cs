﻿using AlmostInstagram.Entities;
using AlmostInstagram.Entities.DataAccess;
using AlmostInstagram.Models;
using AlmostInstagram.Utils;
using AlmostInstagram.Utils.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace AlmostInstagram.Services
{
    public interface ILoginService
    {
        UserModel Login(LoginModel model);
        User Register(RegistrationModel model);
        bool CheckIsUserNameAvailable(string userName);
    }

    public class LoginService : BaseService, ILoginService
    {
        public LoginService(DataContext context): base(context)
        {
        }

        public bool CheckIsUserNameAvailable(string userName)
        {
            if (String.IsNullOrWhiteSpace(userName))
                throw new StatusCodeException((int)HttpStatusCode.BadRequest);

            var user = DB.Users.SingleOrDefault(u => u.UserName == userName);
            if (user != null)
                return false;

            return true;
        }

        public UserModel Login(LoginModel model)
        {
            if (String.IsNullOrWhiteSpace(model.UserName) || String.IsNullOrWhiteSpace(model.Password))
                throw new StatusCodeException((int)HttpStatusCode.NotAcceptable);

            var user = DB.Users.FirstOrDefault(m => m.UserName == model.UserName && m.Password == Common.Sha256(model.Password));

            if (user == null)
                throw new StatusCodeException((int)HttpStatusCode.Unauthorized);

            var payload = new Dictionary<string, object>()
            {
                { "username", user.UserName }
            };

            var token = JWTHelper.EncodeToken(payload);

            var userModel = new UserModel()
            {
                Id = (int)user.Id,
                UserName = user.UserName,
                Token = token
            };
            return userModel;
        }

        public void Logout()
        {

        }

        public User Register(RegistrationModel model)
        {
            if (String.IsNullOrWhiteSpace(model.UserName) ||
                String.IsNullOrWhiteSpace(model.Password) ||
                String.IsNullOrEmpty(model.FirstName) ||
                String.IsNullOrEmpty(model.LastName))
                throw new StatusCodeException((int)HttpStatusCode.BadRequest);

            var user = DB.Users.SingleOrDefault(u => u.UserName == model.UserName);

            if (user != null)
                throw new StatusCodeException((int)HttpStatusCode.ExpectationFailed);

            var newUser = new User()
            {
                FirstName = model.FirstName,
                LastName = model.LastName,
                UserName = model.UserName,
                Password = Common.Sha256(model.Password)
            };
            DB.Users.Add(newUser);

            DB.SaveChanges();

            return newUser;
        }
    }
}
