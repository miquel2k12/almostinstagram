﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AlmostInstagram.Entities.DataAccess;
using AlmostInstagram.Models;
using AlmostInstagram.Utils;
using System.Net;
using AlmostInstagram.Entities;
using Microsoft.EntityFrameworkCore;

namespace AlmostInstagram.Services
{
    public interface IPostService
    {
        List<PostModel> GetPosts(long userId, int pageSize, int pageNbr);
        List<PostModel> GetUserPosts(long userId);
        Post CreatePost(PostFormModel postModel);
        Comment AddComment(CommentAddModel model);
        bool AddOrRemoveLike(PostLikeModel model);
    }

    public class PostService : BaseService, IPostService
    {
        public PostService(DataContext context) : base(context)
        {
        }

        public Comment AddComment(CommentAddModel model)
        {
            var comment = DB.Comments.Add(new Comment()
            {
                Post = DB.Posts.SingleOrDefault(p => p.Id == model.PostId),
                Text = model.Text,
                User = DB.Users.SingleOrDefault(u => u.Id == model.UserId)
            });

            DB.SaveChanges();

            return comment.Entity;
        }

        public bool AddOrRemoveLike(PostLikeModel model)
        {
            if(model.IsLike)
            {
                DB.Likes.Add(new Like()
                {
                    Post = DB.Posts.SingleOrDefault(p => p.Id == model.PostId),
                    User = DB.Users.SingleOrDefault(u => u.Id == model.UserId)
                });
            }
            else
            {
                var like = DB.Likes.SingleOrDefault(l => l.User.Id == model.UserId && l.Post.Id == model.PostId);

                DB.Likes.Remove(like);
            }

            DB.SaveChanges();

            return true;
        }

        public Post CreatePost(PostFormModel postModel)
        {
            var addedPost = DB.Posts.Add(new Post()
            {
                Photo = new Photo()
                {
                    FileBytes = Convert.FromBase64String(postModel.FileBytes.Split(',')[1])
                },
                User = DB.Users.FirstOrDefault(u => u.Id == postModel.UserId),
                UploadDate = DateTime.Now
            });

            DB.SaveChanges();

            return addedPost.Entity;
        }

        public List<PostModel> GetPosts(long userId, int pageSize, int pageNbr)
        {
            var user = DB.Users.Include(u => u.ObservedUsers).FirstOrDefault(u => u.Id == userId);
            if (user == null)
                throw new StatusCodeException((int)HttpStatusCode.BadRequest);

            var followedUsers = user.ObservedUsers.ToList();
            followedUsers.Add(user);
            var posts = DB.Posts.Include(p => p.Photo).Include(p => p.Likers).Include(p => p.Comments).ThenInclude(c => c.User).Where(p => followedUsers.Find(u => u.Id == p.User.Id) != null).Skip(pageNbr*pageSize).Take(pageSize).ToList();

            return posts.Select(p => PostModelFromDbPost(p)).ToList();
        }

        public List<PostModel> GetUserPosts(long userId)
        {
            var posts = DB.Posts.Include(p => p.User).Include(p => p.Photo).Include(p => p.Likers).Include(p => p.Comments).ThenInclude(c => c.User).Where(p => p.User.Id == userId).ToList();

            return posts.Select(p => PostModelFromDbPost(p)).ToList();
        }

        private PostModel PostModelFromDbPost(Post post)
        {
            return new PostModel()
            {
                Id = post.Id,
                Photo = new PhotoModel()
                {
                    Id = post.Photo.Id,
                    FileBytes = post.Photo.FileBytes
                },
                User = new UserDataModel()
                {
                    Id = post.User.Id,
                    FirstName = post.User.FirstName,
                    LastName = post.User.LastName,
                    UserName = post.User.UserName
                },
                Date = post.UploadDate,
                Comments = post.Comments.Select(c => new CommentModel()
                {
                    Text = c.Text,
                    User = new UserDataModel()
                    {
                        Id = c.User.Id,
                        FirstName = c.User.FirstName,
                        LastName = c.User.LastName,
                        UserName = c.User.UserName
                    }
                }).ToList(),
                Likers = post.Likers?.Select(l => new LikeModel()
                {
                    Id = l.Id,
                    User = l.User != null ? new UserDataModel()
                    {
                        Id = l.User.Id,
                        FirstName = l.User.FirstName,
                        LastName = l.User.LastName,
                        UserName = l.User.UserName
                    } : null
                }).ToList()

            };
        }
    }
}
