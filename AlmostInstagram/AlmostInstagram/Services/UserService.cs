﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AlmostInstagram.Entities.DataAccess;
using AlmostInstagram.Models;
using AlmostInstagram.Entities;
using AlmostInstagram.Utils;
using System.Net;
using Microsoft.EntityFrameworkCore;

namespace AlmostInstagram.Services
{
    public interface IUserService
    {
        UserDataModel GetUser(long id);
        List<UserDataModel> GetUsers(string search);
        User FollowUser(long id, long userId);
        User UnfollowUser(long id, long userId);
    }

    public class UserService : BaseService, IUserService
    {
        public UserService(DataContext context) : base(context)
        {
        }

        public User FollowUser(long id, long userId)
        {
            var user = DB.Users.Include(u => u.ObservedUsers).SingleOrDefault(u => u.Id == id);
            if (user == null)
                throw new StatusCodeException((int)HttpStatusCode.BadRequest);

            var followedUser = DB.Users.SingleOrDefault(u => u.Id == userId);
            if (followedUser == null)
                throw new StatusCodeException((int)HttpStatusCode.BadRequest);

            user.ObservedUsers.Add(followedUser);
            DB.Users.Update(user);
            DB.SaveChanges();

            return followedUser;
        }

        public User UnfollowUser(long id, long userId)
        {
            var user = DB.Users.Include(u => u.ObservedUsers).SingleOrDefault(u => u.Id == id);
            if (user == null)
                throw new StatusCodeException((int)HttpStatusCode.BadRequest);

            var followedUser = DB.Users.SingleOrDefault(u => u.Id == userId);
            if (followedUser == null)
                throw new StatusCodeException((int)HttpStatusCode.BadRequest);

            user.ObservedUsers.Remove(followedUser);
            DB.Users.Update(user);
            DB.SaveChanges();

            return followedUser;
        }

        public UserDataModel GetUser(long id)
        {
            var user = DB.Users.Include(u => u.Posts).ThenInclude(p => p.Photo).SingleOrDefault(u => u.Id == id);
            if (user == null)
                throw new StatusCodeException((int)HttpStatusCode.BadRequest);
            var posts = DB.Posts.Include(p => p.User).Include(p => p.Photo).Include(p => p.Likers).Include(p => p.Comments).ThenInclude(c => c.User).Where(p => p.User.Id == id).ToList();
            return UserModelFromDbUser(user, posts);
        }

        public List<UserDataModel> GetUsers(string search)
        {
            if (search == null)
                throw new StatusCodeException((int)HttpStatusCode.BadRequest);

            search = search.ToLower();

            var users = DB.Users.Where(u => u.FirstName.ToLower().StartsWith(search) || u.LastName.ToLower().StartsWith(search) || u.UserName.ToLower().StartsWith(search)).Take(20).ToList();

            return users.Select(u => UserModelFromDbUser(u)).ToList();
        }

        private UserDataModel UserModelFromDbUser(User user, List<Post> posts = null)
        {
            var ps = posts != null ? posts : user.Posts;

            return new UserDataModel()
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                UserName = user.UserName,
                Posts = ps?.Select(p => new PostModel()
                {
                    Id = p.Id,
                    Date = p.UploadDate,
                    Photo = p.Photo != null ? new PhotoModel()
                    {
                        Id = p.Photo.Id,
                        FileBytes = p.Photo.FileBytes
                    } : null,
                    Comments = p.Comments?.Select(c => new CommentModel()
                    {
                        Text = c.Text,
                        User = c.User != null ? new UserDataModel()
                        {
                            Id = c.User.Id,
                            FirstName = c.User.FirstName,
                            LastName = c.User.LastName,
                            UserName = c.User.UserName
                        } : null
                    }).ToList(),
                    Likers = p.Likers?.Select(l => new LikeModel()
                    {
                        Id = l.Id,
                        User = l.User != null ? new UserDataModel()
                        {
                            Id = l.User != null ? l.User.Id : 0,
                            FirstName = l.User?.FirstName,
                            LastName = l.User?.LastName,
                            UserName = l.User?.UserName
                        } : null
                    }).ToList()
                }).ToList(),
                ObservedUsers = user.ObservedUsers?.Select(ou => new UserDataModel()
                {
                    Id = ou.Id,
                    FirstName = ou.FirstName,
                    LastName = ou.LastName,
                    UserName = ou.UserName
                }).ToList()
            };
        }
    }
}
