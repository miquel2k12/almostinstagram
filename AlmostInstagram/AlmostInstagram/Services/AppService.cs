﻿using AlmostInstagram.Entities.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlmostInstagram.Services
{
    public interface IAppService
    {
    }

    public class AppService : BaseService, IAppService
    {
        public AppService(DataContext context): base(context)
        {
        }
    }
}
