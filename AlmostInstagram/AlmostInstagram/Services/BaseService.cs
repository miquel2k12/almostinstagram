﻿using AlmostInstagram.Entities.DataAccess;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;

namespace AlmostInstagram.Services
{
    public class BaseService
    {
        protected DataContext DB;

        public BaseService(DataContext context)
        {
            DB = context;
        }
    }
}
