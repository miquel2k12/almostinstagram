﻿var config = {
    WEB_URL: 'http://localhost:60000/',

    // Date time format for moment.js (http://momentjs.com/docs/#/parsing/string-format/)
    DATETIME_FORMAT: 'YYYY-MM-D hh:mm',
    // Date format for DevExtreme (JavaScript)
    DATE_FORMAT: 'yyyy/MM/dd'
}
module.exports = config;