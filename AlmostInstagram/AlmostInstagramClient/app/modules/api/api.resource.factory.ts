﻿import { UserModel } from './api.models' 

var API_URL = require('../../config').WEB_URL

interface IApiResource<T> extends ng.resource.IResourceClass<ng.resource.IResource<T>> {
}

export class ApiResourceFactory {
    
    public app: IApiResource<any>
    public login: IApiResource<UserModel>
    public post: IApiResource<any>
    public user: IApiResource<any>

    constructor($resource: ng.resource.IResourceService) {

        this.app = $resource(API_URL + 'api/app/:method', {}, {
            'getRoles': { method: 'POST', isArray: false, params: { method: 'roles' } },
            'getActivities': { method: 'POST', isArray: false, params: { method: 'activities' } }
        })
        this.login = $resource(API_URL + 'api/login/:userName', {}, {
            'register': { method: 'PUT' },
            'checkIsUserNameAvailable': {
                method: 'GET', transformResponse: (data) => {
                    return { data: data }
                }}
        })
        this.post = $resource(API_URL + 'api/post/:method', {}, {
            'save': { method: 'POST', isArray: false },
            'getPosts': { method: 'GET', isArray: true },
            'getUserPosts': { method: 'GET', isArray: true, params: { method: 'user' } },
            'addComment': { method: 'PUT', isArray: false, params: { method: 'comment' } },
            'like': { method: 'PUT', isArray: false, params: { method: 'like' } }
        })
        this.user = $resource(API_URL + 'api/user/:id/:method/:userId', {}, {
            'getUsers': { method: 'GET', isArray: true },
            'followUser': { method: 'GET', isArray: false, params: { method: 'follow' } },
            'unfollowUser': { method: 'GET', isArray: false, params: { method: 'unfollow' } }
        })
    }
}