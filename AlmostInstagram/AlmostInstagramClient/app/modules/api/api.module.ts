﻿import 'angular'

import { Api, ApiProvider } from './api.provider'
import { ApiResourceFactory } from './api.resource.factory'
import { ApiHttpRequestInterceptorFactory } from './api.http-request-interceptor.factory'

var moduleName = 'instagram.api'
var module = angular.module(moduleName, [require('angular-resource')])

module.factory('apiResource', ['$resource', ($resource: ng.resource.IResourceService) => new ApiResourceFactory($resource)])
module.factory('httpRequestInterceptor', ['$injector', ($injector: ng.auto.IInjectorService) => new ApiHttpRequestInterceptorFactory($injector)])
module.provider('api', [() => new ApiProvider()])

module.config(['$httpProvider', ($httpProvider: ng.IHttpProvider) => {
    $httpProvider.interceptors.push('httpRequestInterceptor')
}])

export default moduleName
export { Api, ApiProvider }