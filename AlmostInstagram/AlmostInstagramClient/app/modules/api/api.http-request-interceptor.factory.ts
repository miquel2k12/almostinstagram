﻿import { Api } from './api.provider'

export class ApiHttpRequestInterceptorFactory {
    constructor(private injector: ng.auto.IInjectorService) {
        
    }

    public request = (config) => {
        var token = this.injector.get<Api>('api').login.getToken()
        config.headers['JWT'] = token
        return config
    }
}