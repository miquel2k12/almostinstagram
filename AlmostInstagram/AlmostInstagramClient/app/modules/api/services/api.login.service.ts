﻿import { ApiResourceFactory } from '../api.resource.factory'
import { LoginModel, UserModel } from '../api.models'

export interface ILoginService {
    login(loginModel: LoginModel): ng.IPromise<void>
    logout(): ng.IPromise<boolean>
    getToken(): string
    getUser(): UserModel
    isLogged(): boolean
}

export class LoginService implements ILoginService {
    constructor(private apiResource: ApiResourceFactory) {

    }

    public login(loginModel: LoginModel): ng.IPromise<void> {
        return this.apiResource.login.save(loginModel).$promise
            .then(data => {
                localStorage.setItem('user', JSON.stringify(data))
                return
            })
    }

    public logout(): ng.IPromise<boolean> {
        return this.apiResource.login.delete().$promise
            .then(result => {
                localStorage.removeItem('user')
                return true
            })
    }

    public getToken(): string {
        var user = this.getUser()
        return user ? user.token : ''
    }

    public getUser(): UserModel {
        return JSON.parse(localStorage.getItem('user'))
    }

    public isLogged(): boolean {
        var user = this.getUser()
        return user != null
    }
}

export class FakeLoginService implements ILoginService {
    constructor(private $q: ng.IQService) {

    }

    public login(loginModel: LoginModel): ng.IPromise<void> {
        return this.$q.resolve()
    }

    public logout(): ng.IPromise<boolean> {
        return this.$q.resolve(true)
    }

    public getToken(): string {
        return this.getUser().token
    }

    public getUser(): UserModel {
        var fakeUser = new UserModel()
        fakeUser.token = 'verySecureToken'
        return fakeUser
    }

    public isLogged(): boolean {
        return true
    }
}