﻿import { ApiResourceFactory } from '../api.resource.factory'

export interface IAppService {
}

export class AppService implements IAppService {
    constructor(private apiResource: ApiResourceFactory) {

    }
}

export class FakeAppService implements IAppService {
    constructor(private $q: ng.IQService) {

    }
}