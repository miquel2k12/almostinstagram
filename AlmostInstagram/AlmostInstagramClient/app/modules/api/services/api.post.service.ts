﻿import { ApiResourceFactory } from '../api.resource.factory'
import { LoginModel, PostModel, PostFormModel, CommentAddModel, PostLikeModel } from '../api.models'

export interface IPostService {
    createPost(model: PostFormModel): ng.IPromise<any>
    getPosts(userId: number, pageSize: number, pageNbr: number): ng.IPromise<PostModel[]>
    getUserPosts(userId: number): ng.IPromise<PostModel[]>
    addComment(model: CommentAddModel): ng.IPromise<any>
    addOrRemoveLike(model: PostLikeModel): ng.IPromise<boolean>
}

export class PostService implements IPostService {
    constructor(private apiResource: ApiResourceFactory) {

    }

    createPost(model: PostFormModel) {
        return this.apiResource.post.save(model).$promise.then(result => {
            return (<any>result).toJSON()
        })
    }

    getPosts(userId: number, pageSize: number, pageNbr: number) {
        return this.apiResource.post['getPosts']({
            userId: userId,
            pageSize: pageSize,
            pageNbr: pageNbr
        }).$promise
    }

    getUserPosts(userId: number) {
        return this.apiResource.post['getUserPosts']({ userId: userId }).$promise
    }

    addComment(model: CommentAddModel) {
        return this.apiResource.post['addComment'](model).$promise.then(result => {
            return (<any>result).toJSON()
        })
    }

    addOrRemoveLike(model: PostLikeModel) {
        return this.apiResource.post['like'](model).$promise.then(result => {
            return (<any>result).toJSON()
        })
    }
}

export class FakePostService implements IPostService {
    constructor(private $q: ng.IQService) {

    }

    createPost(model: PostFormModel) {
        return this.$q.resolve({})
    }

    getPosts(userId: number, pageSize: number, pageNbr: number) {
        return this.$q.resolve([])
    }

    getUserPosts(userId: number) {
        return this.$q.resolve([])
    }

    addComment(model: CommentAddModel) {
        return this.$q.resolve({})
    }

    addOrRemoveLike(model: PostLikeModel) {
        return this.$q.resolve(false)
    }
}