﻿import { ApiResourceFactory } from '../api.resource.factory'
import { LoginModel, UserDataModel } from '../api.models'

export interface IUserService {
    getUser(id: number): ng.IPromise<UserDataModel>
    getUsers(search: string): ng.IPromise<UserDataModel[]>
    followUser(id: number, userId: number): ng.IPromise<any>
    unfollowUser(id: number, userId: number): ng.IPromise<any>
}

export class UserService implements IUserService {
    constructor(private apiResource: ApiResourceFactory) {

    }

    getUser(id: number) {
        return this.apiResource.user.get({ id: id }).$promise.then(result => {
            return (<any>result).toJSON()
        })
    }

    getUsers(search: string) {
        return this.apiResource.user['getUsers']({ search: search }).$promise
    }

    followUser(id: number, userId: number) {
        return this.apiResource.user['followUser']({ id: id, userId: userId }).$promise.then(result => {
            return (<any>result).toJSON()
        })
    }

    unfollowUser(id: number, userId: number) {
        return this.apiResource.user['unfollowUser']({ id: id, userId: userId }).$promise.then(result => {
            return (<any>result).toJSON()
        })
    }
}

export class FakeUserService implements IUserService {
    constructor(private $q: ng.IQService) {

    }

    getUser(id: number) {
        return this.$q.resolve({})
    }

    getUsers(search: string) {
        return this.$q.resolve([])
    }

    followUser(id: number, userId: number) {
        return this.$q.resolve({})
    }

    unfollowUser(id: number, userId: number) {
        return this.$q.resolve({})
    }
}