﻿import { ApiResourceFactory } from '../api.resource.factory'
import { RegistrationModel } from '../api.models'

export interface IRegistrationService {
    register(model: RegistrationModel): ng.IPromise<any>
    checkIsUserNameAvailable(userName: string): ng.IPromise<boolean>
}

export class RegistrationService implements IRegistrationService {
    constructor(private apiResource: ApiResourceFactory) {

    }

    register(model: RegistrationModel) {
        return this.apiResource.login['register'](model).$promise.then(result => {
            return (<any>result).toJSON()
        })
    }

    checkIsUserNameAvailable(userName: string) {
        return this.apiResource.login['checkIsUserNameAvailable']({ userName: userName }).$promise.then(result => {
            return result.data
        })
    }
}

export class FakeRegistrationService implements IRegistrationService {
    constructor(private $q: ng.IQService) {

    }

    register(model: RegistrationModel) {
        return this.$q.resolve({})
    }

    checkIsUserNameAvailable(userName: string) {
        return this.$q.resolve(false)
    }
}