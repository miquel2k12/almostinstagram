﻿import { ApiResourceFactory } from './api.resource.factory'
import { IAppService, AppService, FakeAppService} from './services/api.app.service'
import { ILoginService, LoginService, FakeLoginService} from './services/api.login.service'
import { IPostService, PostService, FakePostService} from './services/api.post.service'
import { IUserService, UserService, FakeUserService} from './services/api.user.service'
import { IRegistrationService, RegistrationService, FakeRegistrationService} from './services/api.registration.service'

export class Api {
    constructor(public app: IAppService, public login: ILoginService, public post: IPostService, public user: IUserService, public registration: IRegistrationService) {
    }
}

export class ApiProvider {
    private isFakeServices = false

    $get = ['$q', 'apiResource', function ($q: ng.IQService, apiResource: ApiResourceFactory) {
        if (this.isFakeServices)
            return new Api(new FakeAppService($q), new FakeLoginService($q), new FakePostService($q), new FakeUserService($q), new FakeRegistrationService($q))
        else
            return new Api(new AppService(apiResource), new LoginService(apiResource), new PostService(apiResource), new UserService(apiResource), new RegistrationService(apiResource))
    }]

    setFakeServices(isFake) {
        this.isFakeServices = isFake
    }
}