﻿import 'angular'

require('./layout.scss')

export class LayoutDirective implements ng.IDirective {
    constructor() {
    }

    scope = {
    }

    controller = ['$scope', ($scope: ng.IScope) => {

    }]

    restrict = 'E'
    template = require("./layout.tpl.html")
    transclude = true
}