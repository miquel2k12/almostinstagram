﻿import 'angular'

import { I18NProvider } from './i18n/common.i18n.provider'

import { HeaderDirective } from './header/header.directive'
import { LayoutDirective } from './layout/layout.directive'
import { HeightFillDirective } from './common.height-fill.directive'
import { EnterClickDirective } from './enter-click.directive'

var moduleName = 'instagram.common'
var module = angular.module(moduleName, [])

// directives
module.directive('layout', [() => new LayoutDirective()])
module.directive('header', [() => new HeaderDirective()])
module.directive('heightFill', ['$window', ($window: ng.IWindowService) => new HeightFillDirective($window)])
module.directive('enterClick', [() => new EnterClickDirective()])

// services

// providers
module.provider('i18n', [() => new I18NProvider()])

//constants

export default moduleName