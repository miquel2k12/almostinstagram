var translate = (collection: string, word: string) => {
    var resource = global['resource']
    if (resource && resource[collection] && resource[collection][word]) return resource[collection][word]
    else return word
}

export var i18n = translate