﻿import 'angular'
import { i18n } from '../../common/i18n/common.i18n'
import { UserModel, UserDataModel } from '../../api/api.models'

require('./header.scss')
import { Api } from  '../../api/api.module'

interface HeaderScope extends ng.IScope {
    user: UserModel

    search: string
    searchResults: UserDataModel[]
    lostFocus: () => void
    gainedFocus: () => void
}

export class HeaderDirective implements ng.IDirective {
    constructor() {

    }


    restrict = 'E'
    link = (scope: ng.IScope, element: JQuery) => {
        scope['setLocale'] = (locale) => {
            localStorage.setItem('locale', locale)
            location.href = '#/logout'
            window.location.reload()
        }
    }
    controller = ['$scope', '$timeout', 'api', ($scope: HeaderScope, $timeout: ng.ITimeoutService, api: Api) => {
        $scope.user = api.login.getUser();

        var timeout

        $scope.$watch('search', value => {
            if (timeout)
                $timeout.cancel(timeout)

            if (!value) {
                $scope.searchResults = null
                return
            }

            timeout = $timeout(() => {
                api.user.getUsers(<string>value).then(result => {
                    $scope.searchResults = result
                })
            }, 1000)
        })

        $scope.$watch('searchResults', value => {
            if (!value) {
                angular.element('.result-list').hide()
            } else {
                angular.element('.result-list').show()
            }
        })

        $scope.lostFocus = () => {
            $timeout(() => {
                angular.element('.result-list').hide({
                    easing: 'swing',
                    duration: 500
                })
            }, 100)
        }

        $scope.gainedFocus = () => {
            if ($scope.searchResults)
                angular.element('.result-list').show()
        }
    }]
    template = require("./header.tpl.html")
    replace = true
}