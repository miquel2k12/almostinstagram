﻿import 'angular'

export class EnterClickDirective implements ng.IDirective {
    constructor() {

    }

    restrict = 'A'
    link = (scope: ng.IScope, element: JQuery, attrs) => {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.enterClick);
                });

                event.preventDefault();
            }
        });
    }
}