﻿import 'angular'
import { i18n } from '../../common/i18n/common.i18n'
import { PostModel, CommentAddModel, UserModel, PostLikeModel, CommentModel, LikeModel } from '../../api/api.models'
import { CommonHelper } from '../../../helpers/common-helper'

require('./post.scss')
import { Api } from  '../../api/api.module'

interface PostScope extends ng.IScope {
    user: UserModel

    article: PostModel
    dateDifference: string
    newComment: string
    isLiked: boolean

    changeLike: () => void
    submitComment: () => void
}

export class PostDirective implements ng.IDirective {
    constructor() {

    }
    scope = {
        article: '='
    }

    restrict = 'E'
    controller = ['$scope', '$interval', 'api', ($scope: PostScope, $interval: ng.IIntervalService, api: Api) => {
        $scope.dateDifference = CommonHelper.countDateDifference($scope.article.date)
        if (api.login.isLogged())
            $scope.user = api.login.getUser()

        for (var i = 0; i < $scope.article.likers.length; i++) {
            var like = $scope.article.likers[i]
            if (like.user.id == $scope.user.id) {
                $scope.isLiked = true
                break
            }
        }

        var isSubmitted = false

        $scope.submitComment = () => {
            if (!isSubmitted && $scope.newComment.length > 0) {
                isSubmitted = true

                var model = new CommentAddModel()
                model.userId = $scope.user.id
                model.postId = $scope.article.id
                model.text = $scope.newComment

                api.post.addComment(model).then(result => {
                    if (result) {
                        var comment = new CommentModel()
                        comment.text = $scope.newComment
                        comment.user = {
                            userName: $scope.user.userName,
                            id: $scope.user.id,
                            firstName: null,
                            lastName: null,
                            posts: null,
                            observedUsers: null
                        }

                        $scope.article.comments.push(comment)
                        $scope.newComment = ''
                    }
                })
            }
            console.log($scope.article)
        }

        $scope.changeLike = () => {
            $scope.isLiked = !$scope.isLiked

            var model = new PostLikeModel()
            model.postId = $scope.article.id
            model.userId = $scope.user.id
            model.isLike = $scope.isLiked

            api.post.addOrRemoveLike(model).then(result => {
                if (result) {
                    if ($scope.isLiked) {
                        var likeModel = new LikeModel()
                        likeModel.user = {
                            id: $scope.user.id,
                            userName: $scope.user.userName,
                            firstName: null,
                            lastName: null,
                            posts: null,
                            observedUsers: null
                        }

                        $scope.article.likers.push(likeModel)

                    }
                    else
                        for (var i = 0; i < $scope.article.likers.length; i++) {
                            var like = $scope.article.likers[i]
                            if (like.user.id == $scope.user.id) {
                                $scope.article.likers.splice(i, 1)
                                break
                            }
                        }
                }
            })
        }
    }]
    template = require("./post.tpl.html")
    replace = true
}