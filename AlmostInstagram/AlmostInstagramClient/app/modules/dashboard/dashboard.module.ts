﻿import 'angular'
import 'angular-ui-router'

import { default as apiModule, Api } from  './../api/api.module'
import { default as commonModule } from  './../common/common.module'

import { PostDirective } from './post/post.directive'

import { DashboardController } from './dashboard.controller'

require('./dashboard.scss')

var moduleName = 'instagram.dashboard'
var module = angular.module(moduleName, [require('angular-animate'), 'ui.router', commonModule, apiModule])

module.controller('DashboardController', ['$scope', '$state', '$location', 'api', ($scope, $state: angular.ui.IStateService, $location: ng.ILocationService, api: Api, usSpinnerService) => new DashboardController($scope, $state, $location, api)])

module.directive('post', [() => new PostDirective()])

module.config(['$stateProvider', ($stateProvider: angular.ui.IStateProvider) => {
    $stateProvider.state('dashboard', {
        url: '/dashboard',
        controller: 'DashboardController',
        template: require('./dashboard.tpl.html')
    })
}])

export default moduleName