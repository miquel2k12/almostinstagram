﻿import { Api } from  './../api/api.module'
import { i18n } from '../common/i18n/common.i18n'
import { PostModel, UserModel } from '../api/api.models'

export interface DashboardScope extends ng.IScope {
    posts: PostModel[]
    user: UserModel
}

export class DashboardController {
    constructor(private $scope: DashboardScope, $state: angular.ui.IStateService, $location: ng.ILocationService, private api: Api) {
        $scope.posts = []

        if (api.login.isLogged())
            $scope.user = api.login.getUser()

        this.getPosts()
    }

    getPosts(pageSize: number = 20, pageNbr: number = 0) {
        this.api.post.getPosts(this.$scope.user.id, pageSize, pageNbr).then(result => {
            result.forEach(post => {
                this.$scope.posts.push(post)
            })
        }).catch(rej => {
            console.log(rej)
        })
    }
}

