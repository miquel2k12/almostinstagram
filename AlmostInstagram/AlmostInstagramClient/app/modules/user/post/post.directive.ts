﻿import 'angular'
import { i18n } from '../../common/i18n/common.i18n'
import { PostModel } from '../../api/api.models'
import { CommonHelper } from '../../../helpers/common-helper'

require('./post.scss')
import { Api } from  '../../api/api.module'

interface PostScope extends ng.IScope {
    article: PostModel
    dateDifference: string
}

export class PostDirective implements ng.IDirective {
    constructor() {

    }
    scope = {
        article: '='
    }

    restrict = 'E'
    controller = ['$scope', '$interval', 'api', ($scope: PostScope, $interval: ng.IIntervalService, api: Api) => {
        console.log($scope.article)
    }]
    template = require("./post.tpl.html")
    replace = true
}