﻿import { Api } from  './../api/api.module'
import { i18n } from '../common/i18n/common.i18n'
import { PostModel, UserModel, UserDataModel } from '../api/api.models'

export interface UserScope extends ng.IScope {
    loggedUser: UserModel
    loggedUserData: UserDataModel
    user: UserDataModel

    logout: () => void
    follow: () => void
    unfollow: () => void
    isAlreadyFollowing: boolean
}

export class UserController {
    constructor(private $scope: UserScope, $state: angular.ui.IStateService, $stateParams: angular.ui.IStateParamsService, $location: ng.ILocationService, private api: Api, Notification) {
        if (api.login.isLogged())
            $scope.loggedUser = api.login.getUser()

        if ($stateParams['id']) {
            api.user.getUser($stateParams['id']).then(result => {
                $scope.user = result
                return
            }).then(() => {
                if ($scope.loggedUser.id != $scope.user.id) {
                    api.user.getUser($scope.loggedUser.id).then(result => {
                        $scope.loggedUserData = result

                        checkIsFollowing()
                    })
                }
            })
        }

        $scope.logout = () => {
            localStorage.removeItem('user')
            $state.go('login')
        }

        $scope.follow = () => {
            api.user.followUser($scope.loggedUser.id, $scope.user.id).then(result => {
                if (result) {
                    Notification.success(i18n('Common', 'UserFollowed'))
                    $scope.isAlreadyFollowing = true
                }
            })
        }

        $scope.unfollow = () => {
            api.user.unfollowUser($scope.loggedUser.id, $scope.user.id).then(result => {
                if (result) {
                    Notification.success(i18n('Common', 'Unfollowed'))
                    $scope.isAlreadyFollowing = false
                }
            })
        }

        var checkIsFollowing = () => {
            $scope.loggedUserData.observedUsers.forEach(u => {
                if (u.id == $scope.user.id)
                    $scope.isAlreadyFollowing = true
            })
        }
    }
    
}

