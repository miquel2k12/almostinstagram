﻿import 'angular'
import 'angular-ui-router'

import { default as apiModule, Api } from  './../api/api.module'
import { default as commonModule } from  './../common/common.module'

import { UserController } from './user.controller'

import { PostDirective } from './post/post.directive'

require('./user.scss')

var moduleName = 'instagram.user'
var module = angular.module(moduleName, [require('angular-animate'), 'ui.router', commonModule, apiModule])

module.controller('UserController', ['$scope', '$state', '$stateParams', '$location', 'api', 'Notification', ($scope, $state: angular.ui.IStateService, $stateParams: angular.ui.IStateParamsService, $location: ng.ILocationService, api: Api, Notification) => new UserController($scope, $state, $stateParams, $location, api, Notification)])

module.directive('userPost', [() => new PostDirective()])

module.config(['$stateProvider', ($stateProvider: angular.ui.IStateProvider) => {
    $stateProvider.state('user', {
        url: '/user/:id',
        controller: 'UserController',
        template: require('./user.tpl.html')
    })
}])

export default moduleName