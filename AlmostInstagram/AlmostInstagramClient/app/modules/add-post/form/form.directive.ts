﻿import 'angular'
import { i18n } from '../../common/i18n/common.i18n'
import { UserModel, UserDataModel, PostFormModel } from '../../api/api.models'

require('./form.scss')
import { Api } from  '../../api/api.module'

declare var vm: any

interface FormScope extends ng.IScope {
    loggedUser: UserModel

    submit: () => void
    ctrl: any
    post: PostFormModel
}

export class FormDirective implements ng.IDirective {
    constructor() {

    }

    restrict = 'EAC'
    controller = ($scope: FormScope, $state: angular.ui.IStateService, $timeout: ng.ITimeoutService, api: Api, Notification) => {
        $scope.post = <PostFormModel>{}

        if (api.login.isLogged())
            $scope.loggedUser = api.login.getUser()

        $scope.submit = () => {
            $scope.post.userId = $scope.loggedUser.id

            if (!$scope.ctrl.file) {
                Notification.error(i18n('Common', 'AddTextOrPhoto'))
                return
            }

            var reader = new FileReader()

            reader.onload = (e: any) => {
                $scope.post.fileBytes = e.target.result
                api.post.createPost($scope.post).then(result => {
                    if (result) {
                        $state.go('dashboard')
                    }
                })
            }

            reader.readAsDataURL($scope.ctrl.file)
        }

        $scope.$watch('ctrl.file', value => {
            if (value)
                $scope.ctrl.isPreview = true
            else
                $scope.ctrl.isPreview = false
        })
    }
    
    template = require("./form.tpl.html")
    replace = true
}