﻿import { Api } from  './../api/api.module'
import { i18n } from '../common/i18n/common.i18n'
import { PostModel, UserModel, UserDataModel } from '../api/api.models'

declare var vm

export interface AddPostScope extends ng.IScope {
    loggedUser: UserModel

    post: PostModel

    postForm: ng.IFormController
    file: any
    submit: () => any

    isPreview: boolean
}

export class AddPostController {
    constructor(private $scope: AddPostScope, $state: angular.ui.IStateService, $location: ng.ILocationService, private api: Api, private Upload) {
        
        if (api.login.isLogged())
            $scope.loggedUser = api.login.getUser()
    }

}

