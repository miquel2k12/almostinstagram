﻿import 'angular'
import 'angular-ui-router'

import { default as apiModule, Api } from  './../api/api.module'
import { default as commonModule } from  './../common/common.module'

import { AddPostController } from './add-post.controller'

import { FormDirective } from './form/form.directive'

require('./add-post.scss')

var moduleName = 'instagram.addpost'
var module = angular.module(moduleName, [require('angular-animate'), 'ui.router', require('ng-file-upload'), commonModule, apiModule])

module.controller('AddPostController', ['$scope', '$state', '$location', 'api', 'Upload', ($scope, $state: angular.ui.IStateService, $location: ng.ILocationService, api: Api, Upload) => new AddPostController($scope, $state, $location, api, Upload)])

module.directive('postForm', [() => new FormDirective()])

module.config(['$stateProvider', ($stateProvider: angular.ui.IStateProvider) => {
    $stateProvider.state('addPost', {
        url: '/addpost',
        controller: 'AddPostController',
        template: require('./add-post.tpl.html')
    })
}])

export default moduleName