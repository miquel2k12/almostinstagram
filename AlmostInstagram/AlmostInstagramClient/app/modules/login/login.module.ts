﻿import 'angular'
import 'angular-ui-router'

import { default as apiModule, Api } from  './../api/api.module'
import { default as commonModule } from  './../common/common.module'

import { LoginController } from './login.controller'

require('./login.scss')

var moduleName = 'instagram.login'
var module = angular.module(moduleName, [require('angular-animate'), 'ui.router', commonModule, apiModule])

module.controller('LoginController', ['$scope', '$state', '$location', 'api', ($scope, $state: angular.ui.IStateService, $location: ng.ILocationService, api: Api) => new LoginController($scope, $state, $location, api)])

module.config(['$stateProvider', ($stateProvider: angular.ui.IStateProvider) => {
    $stateProvider.state('login', {
        url: '/login',
        controller: 'LoginController',
        template: require('./login.tpl.html')
    }).state('logout', {
        url: '/logout',
        controller: ['$state', 'api', ($state: angular.ui.IStateService, api: Api) => {
            api.login.logout()
                .then(result => {
                    $state.go('login').then(() => {
                        location.reload()
                    })
                }).catch(() => {
                    $state.go('login').then(() => {
                        location.reload()
                    })
                })
        }]
    })
}])

export default moduleName