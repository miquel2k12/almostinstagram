﻿import { Api } from  './../api/api.module'
import { LoginModel } from '../api/api.models'
import { i18n } from '../common/i18n/common.i18n'

export interface LoginScope extends ng.IScope {
    date: Date
    showError: string
    showInfo: string
    loginModel: LoginModel
    logIn(): ng.IPromise<void>
    loginForm: ng.IFormController
    setLocale(locale: string)

    password: string
    password2: string
}

export class LoginController {
    constructor($scope: LoginScope, $state: angular.ui.IStateService, $location: ng.ILocationService, api: Api) {
        $scope['i18n'] = i18n

        var sessionGuid = $location.search().sessionGuid
        
        var getLocale = () => {
            var locale = localStorage.getItem('locale')
            if (!locale) locale = 'pl-PL'
            return locale
        }

        $scope.date = new Date()
        $scope.logIn = () => {
            if ($scope.loginForm.$invalid) {
                $scope.showError = i18n('Common', 'LoginErrorText')
            } else {
                return api.login.login($scope.loginModel).then(result => {
                    $state.go('dashboard')
                }).catch(reason => {
                    $scope.showError = i18n('Common', 'LoginErrorText')
                })
            }
        }

        $scope.setLocale = (locale) => {
            localStorage.setItem('locale', locale)
            window.location.reload()
        }
    }
}

