﻿import { Api } from  './../api/api.module'
import { RegistrationModel } from '../api/api.models'
import { i18n } from '../common/i18n/common.i18n'

export interface RegistrationScope extends ng.IScope {
    date: Date
    showError: string
    showInfo: string
    registrationModel: RegistrationModel
    register(): ng.IPromise<void>
    registerForm: ng.IFormController

    checkUserName: (e) => void
    checkPasswords: (e) => void

    password: string
    secondPassword: string
    isAvailable: boolean
    secondPasswordIncorrect: boolean
}

export class RegistrationController {
    constructor($scope: RegistrationScope, $state: angular.ui.IStateService, $location: ng.ILocationService, api: Api, Notification) {
        $scope['i18n'] = i18n
        $scope.registrationModel = <RegistrationModel>{}
        $scope.isAvailable = true

        var sessionGuid = $location.search().sessionGuid
        var timeout

        var getLocale = () => {
            var locale = localStorage.getItem('locale')
            if (!locale) locale = 'pl-PL'
            return locale
        }

        $scope.date = new Date()
        $scope.register = () => {
            if ($scope.registerForm.$invalid) {
                $scope.showError = i18n('Common', 'RegistrationErrorText')
            } else {
                return api.registration.register($scope.registrationModel).then(result => {
                    Notification.success(i18n('Common', 'RegistrationSuccess'))
                    $state.go('login')
                }).catch(reason => {
                    Notification.error(i18n('Common', 'RegistrationErrorText'))
                    $scope.showError = i18n('Common', 'RegistrationErrorText')
                })
            }
        }

        $scope.checkUserName = e => {
            clearTimeout(timeout)
            if ($scope.registrationModel.userName && $scope.registrationModel.userName.length > 0) {
                timeout = setTimeout(() => {
                    api.registration.checkIsUserNameAvailable($scope.registrationModel.userName).then(result => {
                        $scope.isAvailable =  (<any>result === 'true')
                    })
                }, 1000)
            }
        }

        $scope.checkPasswords = e => {
            if ($scope.registrationModel.password && $scope.registrationModel.password.length > 0 &&
                $scope.secondPassword && $scope.secondPassword.length > 0) {
                console.log($scope.secondPassword)
                $scope.secondPasswordIncorrect = ($scope.registrationModel.password != $scope.secondPassword)
            }
        }
    }
}

