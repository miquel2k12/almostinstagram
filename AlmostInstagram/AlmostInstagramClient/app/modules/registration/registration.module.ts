﻿import 'angular'
import 'angular-ui-router'

import { default as apiModule, Api } from  './../api/api.module'
import { default as commonModule } from  './../common/common.module'

import { RegistrationController } from './registration.controller'

require('./registration.scss')

var moduleName = 'instagram.register'
var module = angular.module(moduleName, [require('angular-animate'), 'ui.router', commonModule, apiModule])

module.controller('RegistrationController', ['$scope', '$state', '$location', 'api', 'Notification', ($scope, $state: angular.ui.IStateService, $location: ng.ILocationService, api: Api, Notification) => new RegistrationController($scope, $state, $location, api, Notification)])

module.config(['$stateProvider', ($stateProvider: angular.ui.IStateProvider) => {
    $stateProvider.state('register', {
        url: '/register',
        controller: 'RegistrationController',
        template: require('./registration.tpl.html')
    })
}])

export default moduleName