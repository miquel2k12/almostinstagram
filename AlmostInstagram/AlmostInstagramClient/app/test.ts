﻿import { ApiProvider } from './modules/api/api.module'
import { instagram } from './app'

instagram.config(['apiProvider', (apiProvider: ApiProvider) => {
    apiProvider.setFakeServices(true)
}])