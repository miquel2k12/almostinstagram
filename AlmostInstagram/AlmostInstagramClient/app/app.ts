﻿import 'angular'

require('./app.scss')
require('../js/angular-gridster.js')
require('./ie11.js')
require('angular-ui-notification')
require('angular-ui-notification/dist/angular-ui-notification.min.css')

import { Api, default as apiModule } from  './modules/api/api.module'
import { default as commonModule } from './modules/common/common.module'
import loginModule from './modules/login/login.module'
import dashboardModule from './modules/dashboard/dashboard.module'
import userModule from './modules/user/user.module'
import addPostModule from './modules/add-post/add-post.module'
import registrationModule from './modules/registration/registration.module'

export var instagram = angular.module('instagram', ['ui-notification', commonModule, apiModule, loginModule, dashboardModule, userModule, addPostModule, registrationModule])

var resourcesDownloaded = false

instagram.controller("appController", ['$rootScope', '$scope', '$timeout', '$state', 'i18n', 'api', 'Notification', ($rootScope: ng.IRootScopeService, $scope: ng.IScope, $timeout: ng.ITimeoutService, $state: angular.ui.IStateService, i18n, api: Api, Notification) => {
    if (!api.login.isLogged() && ['login', 'register'].indexOf($state.current.name) < 0) $state.go('login')
    else {
        var user = api.login.getUser()
    }

    $scope['resourcesDownloaded'] = false
    $rootScope['i18n'] = i18n

    $timeout(() => {
        $rootScope.$broadcast("showLoadingIndicator")
    }, 0)

    var resourcesInterval = setInterval(() => {
        if (resourcesDownloaded) {
            $scope['resourcesDownloaded'] = true
            $rootScope.$broadcast("hideLoadingIndicator")
            if (!$rootScope.$$phase) $rootScope.$apply()
            clearInterval(resourcesInterval)
        }
    }, 100)
}])

instagram.config(['i18nProvider', 'NotificationProvider', (i18nProvider, NotificationProvider) => {
    var locale = localStorage.getItem('locale')
    if (locale != 'en-GB' && locale != 'pl-PL') locale = 'pl-PL'

    i18nProvider.setLanguage(locale)
    $.ajax(require('./config').WEB_URL + 'api/app/' + locale + '/resources', {
        method: 'GET',
        success: (data) => {
            i18nProvider.setResources(JSON.parse(data))
            resourcesDownloaded = true
        }
    })

    NotificationProvider.setOptions({
        delay: 5000,
        positionX: 'right',
        positionY: 'top'
    })
}])