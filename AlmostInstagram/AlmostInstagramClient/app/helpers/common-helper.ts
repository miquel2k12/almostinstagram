﻿import * as moment from 'moment'

export class CommonHelper {
    static countDateDifference(netDate) {
        var now = new Date()
        var date = moment(netDate).toDate()
        if (now.getFullYear() - date.getFullYear() > 0) {
            return (now.getFullYear() - date.getFullYear()) + 'y'
        } else if (now.getMonth() - date.getMonth() > 0) {
            return (now.getMonth() - date.getMonth()) + 'm'
        } else if (now.getDate() - date.getDate() > 0) {
            return (now.getDate() - date.getDate()) + 'd'
        } else if (now.getHours() - date.getHours() > 0) {
            return (now.getHours() - date.getHours()) + 'h'
        } else if (now.getMinutes() - date.getMinutes() > 0) {
            return (now.getMinutes() - date.getMinutes()) + 'min'
        } else {
            return (now.getSeconds() - date.getSeconds()) + 's'
        }
    }
}