﻿var path = require('path');
var webpack = require('webpack');

var CopyWebpackPlugin = require('copy-webpack-plugin');
var ProgressBarPlugin = require('progress-bar-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    context: __dirname,
    entry: {
        app: './build/entry.ts',
    },
    output: {
        filename: 'js/bundle.js',
        path: __dirname + '/build/client/'
    },
    resolve: {
        root: [path.join(__dirname, "node_modules"), path.join(__dirname, "bower_components")],
        extensions: ['', '.ts', '.js']
    },
    devtool: "#source-map",
    plugins: [
        new webpack.ResolverPlugin(
            new webpack.ResolverPlugin.DirectoryDescriptionFilePlugin('.bower.json', ['main'])
        ),
        new CopyWebpackPlugin([
            {
                from: './css',
                to: 'css',
                ignore: ['img/*']
            }, {
                from: './images',
                to: 'images'
            }
        ]),
        new ExtractTextPlugin('css/style.css'),
        new ProgressBarPlugin()
    ],
    module: {
        loaders: [
            { test: /\.ts$/, loader: 'ts', exclude: /node_modules/ },
            { test: /\.html$/, loader: 'raw' },
            { test: /\.scss$/, loader: ExtractTextPlugin.extract('style', 'css?sourceMap!sass?sourceMap') },
            { test: /\.css$/, loaders: ['style', 'css?sourceMap'] },
            { test: /\.js$/, loader: 'script', exclude: /node_modules/ },
            { test: /\.(jpe?g|png|gif|svg)$/i, loader: 'file?hash=sha512&digest=hex&publicPath=../&name=images/[hash].[ext]' }
        ],
    }
}